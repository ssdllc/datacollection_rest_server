
'use strict';

const common = require('../helpers/common.js'),
    datatables = require('../helpers/datatables.js'),
    db = require('../requests/db.js'),
    winston = require('winston');

const assetInventoryDatatable = function (facility, aoData, clientRes) {

    const fieldsToSearch = [];
    const alias = "assetInventory";

    const dataQuery =
        "SELECT META(assetInventory).id as id, " +
        "uniformatII.title as uniformatIITitle, " +
        "uniformatII.assetType || assetInventory.`number` as assetType, " +
        "uniformatII.assetTypeDescription, " +
        "assetInventory.data[0].attributes[0].`value` as count, " +
        "assetInventory.data[1].attributes[0].`value` as units, " +
        "unit.abbreviation as unitType, " +
        "material.displayValue as materialDisplay, " +
        "material.abbreviation as materialDescription, " +
        "assetInventory.data[3].attributes[0].`value` as age, " +
        "assetInventory.data[4].attributes[0].`value` || ' ' || assetInventory.data[4].attributes[1].`value` as size, " +
        "assetInventory.data[5].attributes[0].`value` as shape " +
        "FROM dc_db assetInventory " +
        "JOIN dc_db uniformatII ON KEYS assetInventory.uniformatII_ID " +
        "JOIN dc_db unit ON KEYS assetInventory.data[1].attributes[1].`value` " +
        "JOIN dc_db material ON KEYS assetInventory.data[2].attributes[0].`value` " +
        "WHERE assetInventory._sync AND " +
        "assetInventory.type = 'data_assetInventory' " +
        "AND assetInventory.facility = '" + facility + "' ";

    const recordCountQuery =
        "SELECT COUNT(META(assetInventory).id) as count " +
        "FROM dc_db assetInventory " +
        "WHERE assetInventory._sync AND " +
        "assetInventory.type = 'data_assetInventory' " +
        "AND assetInventory.facility = '" + facility + "' ";

    datatables.commonDatatable(dataQuery, recordCountQuery, alias, fieldsToSearch, aoData, clientRes);
};

const getValues = function(qResults) {
    var retValues = [];
    for (var i=0; i < qResults.length; i++) {
        var rec = qResults[i];
        rec.value["id"]=rec.id;
        retValues.push(rec.value);
    }
    return retValues;
};

const assetInventoryCollection= function (clientReq, clientRes){
    let viewQuery = db.couchbaseViewQuery;
    let facilityID = clientReq.body.facility;
    var query = viewQuery
        .from('lookUps', 'viewAssetInventories')
        .stale(viewQuery.Update.BEFORE)
        .range([facilityID],[facilityID,{}],true);

    db.viewQuery(query, function(err, results) {
        var resValues = getValues(results);
        clientRes.json(resValues);
    });
};

const getAssetInventory = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'data_assetInventory', clientRes);
    else if (clientReq.body.aoData)
        assetInventoryDatatable(clientReq.body.facility, clientReq.body.aoData, clientRes);
    else
        assetInventoryCollection(clientReq, clientRes);
};

/* PUBLIC API */
module.exports = {
    createFacility: common.createDocAndRespond,
    getAssetInventory: getAssetInventory,
    updateFacility: common.updateDocAndRespond,
    deleteFacility: common.deleteDocAndRespond
};
