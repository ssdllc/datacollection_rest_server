
'use strict';

const common = require('../helpers/common.js'),
    winston = require('winston');

const materialsList = function (clientRes) {

    const query =
        "SELECT META(material).id as id, " +
        "material.displayValue " +
        "FROM dc_db material " +
        "WHERE material.type = 'material'";

    common.n1qlRequestAndRespond(query, clientRes);
};

const getMaterials = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'material', clientRes);
    else
        materialsList(clientRes);
};

/* PUBLIC API */
module.exports = {
    getMaterials: getMaterials
};
