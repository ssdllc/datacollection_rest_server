
'use strict';

const db = require('../requests/db.js'),
    bcrypt = require('bcrypt-nodejs'),
    moment = require('moment'),
    winston = require('winston');

    //winston.add(winston.transports.File, { filename: 'errors.log', level: 'error' });

const getCurrentUser = function (clientReq, clientRes) {
    clientRes.json(db.getLoggedInUser());
};

const loggedIn = function (clientReq, callback) {

    // session does exist
    if (clientReq.session && clientReq.session.user_id) {

        // update lastRequestTime
        clientReq.session.lastRequestTime = moment();

        var userQuery =
            "SELECT META(x).id AS id, x.nameFirst, x.nameLast " +
            "FROM dc_db x " +
            "WHERE META(x).id = '"+clientReq.session.user_id+"'";

        db.n1qlRequest(userQuery, function(err, res) {

            if (err) {
                callback({err:err, code:"L2"}, null);
                winston.log('error', err);
                return;
            }

            db.setLoggedInUser(res[0]);

            callback(null, true);
        });
    }
    // session does not exist
    else callback(null, false);
};

const login = function (clientReq, clientRes) {

    var data = clientReq.body;

    if (data.username && data.password) {

        var userQuery =
            "SELECT META(x).id AS id, x.nameFirst, x.nameLast " +
            "FROM dc_db x " +
            "WHERE x.email = '"+data.username+"' " +
            "AND x.type = 'user'";

        db.n1qlRequest(userQuery, function (err, res) {

            if (err) {
                clientRes.status(500).json({err:err, code:"L3"});
                winston.log('error', err);
                return;
            }

            var user = res[0];

            /* if user exists */
            if (user) {

                var userCredentialsQuery =
                    "SELECT x.hashed_password " +
                    "FROM dc_db x " +
                    "WHERE x.user_id = '" + user.id + "' " +
                    "AND x.type = 'userCredentials'";

                db.n1qlRequest(userCredentialsQuery, function (err, res) {

                    if (err) {
                        clientRes.status(500).json({err:err, code:"L4"});
                        winston.log('error', err);
                        return;
                    }

                    var userCredentials = res[0];

                    bcrypt.compare(data.password, userCredentials.hashed_password, function(err, res) {

                        /* deleting passwords */
                        delete data.password;
                        delete userCredentials.hashed_password;

                        if (err) {
                            clientRes.status(500).json({err:err, code:"L6"});
                            winston.log('error', err);
                            return;
                        }

                        // if passwords do not match
                        if (!res) {
                            clientRes.status(403).send('Incorrect username and/or password.'); //incorrect password
                            winston.log('error', err);
                            return;
                        }

                        clientReq.session.loginTime = moment();
                        clientReq.session.lastRequestTime = moment();
                        clientReq.session.user_id = user.id;

                        clientRes.json(user);
                    });
                });
            } else {
                clientRes.status(403).send('Incorrect username and/or password.');  //incorrect username
                winston.log('error', 'error 403, incorrect username or password')
            }
        });
    } else {
        clientRes.status(400).send('Please enter both a username and a password to login.');
        winston.log('error', 'error 40, please enter both username and password')
    }
};

const logout = function (clientReq, clientRes) {

    clientReq.session.destroy(function(err) {

        if (err && err.message !== "The key does not exist on the server") {
            clientRes.status(500).json({err:err, code:"L7"});
            winston.log('error', err);
            return;
        }

        clientRes.json({"message":'logout successful'});
    });
};

/* PUBLIC API */
module.exports = {
    logout: logout,
    login: login,
    loggedIn: loggedIn,
    getCurrentUser: getCurrentUser
};
