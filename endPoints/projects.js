
'use strict';

const common = require('../helpers/common.js'),
    n1qlQueryDefinition = require('../helpers/n1qlQueryDefinition.js'),
    datatables = require('../helpers/datatables.js'),
    db = require('../requests/db.js'),
    winston = require('winston');


const projectsDatatable = function (aoData, clientRes) {

    const fieldsToSearch = ["title", "location", "`value`"];
    const alias = "project";

    const attributesToSelect = ["title","date","location","`value`"];
    const whereClauseAttributeValues = {"type":"project"};

    const dataQuery = n1qlQueryDefinition.dataQuery(alias, attributesToSelect,whereClauseAttributeValues);

    const recordCountQuery = n1qlQueryDefinition.recordCountQuery(alias, whereClauseAttributeValues);

    //const dataQuery =
    //    "SELECT META(project).id as id, " +
    //    "project.title, project.date, project.location, project.`value` " +
    //    "FROM dc_db project " +
    //    "WHERE project._sync AND " +
    //    "project.type = 'project' ";
    //
    //const recordCountQuery =
    //    "SELECT COUNT(META(project).id) as count " +
    //    "FROM dc_db project " +
    //    "WHERE project._sync AND " +
    //    "project.type = 'project' ";

    datatables.commonDatatable(dataQuery, recordCountQuery, alias, fieldsToSearch, aoData, clientRes);
};

const getValues = function(qResults) {
    var retValues = [];
    for (var i=0; i < qResults.length; i++) {
        var rec = qResults[i];
        rec.value["id"]=rec.id;
        retValues.push(rec.value);
    }
    return retValues;
};

const projectsCollection = function(clientRes){
    let viewQuery = db.couchbaseViewQuery;
    var query = viewQuery
        .from('lookUps', 'viewProjects')
        .stale(viewQuery.Update.BEFORE);


    db.viewQuery(query, function(err, results) {
        var resValues = getValues(results);
        clientRes.json(resValues);
    });
};

const getProjects = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'project', clientRes);
    else if (clientReq.body.aoData)
        projectsDatatable(clientReq.body.aoData, clientRes);
    else
        projectsCollection(clientRes);


};

/* PUBLIC API */
module.exports = {
    createProject: common.createDocAndRespond,
    getProjects: getProjects,
    updateProject: common.updateDocAndRespond,
    deleteProject: common.deleteDocAndRespond
};
