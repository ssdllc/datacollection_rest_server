
'use strict';

const common = require('../helpers/common.js'),
    winston = require('winston');

const unitsList = function (grouping, clientRes) {

    let query =
        "SELECT META(unit).id as id, " +
        "unit.displayValue " +
        "FROM dc_db unit " +
        "WHERE unit.type = 'unit'";

    if (grouping)
        query += " AND unit.grouping = '" + grouping + "'";

    common.n1qlRequestAndRespond(query, clientRes);
};

const getUnits = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'unit', clientRes);
    else
        unitsList(clientReq.body.grouping, clientRes);
};

/* PUBLIC API */
module.exports = {
    getUnits: getUnits
};
