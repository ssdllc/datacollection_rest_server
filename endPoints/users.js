
'use strict';

const common = require('../helpers/common.js'),
    uuid = require('node-uuid'),
    db = require('../requests/db.js'),
    bcrypt = require('bcrypt-nodejs'),
    datatables = require('../helpers/datatables.js'),
    documentStructure = require('../helpers/documentStructure.js'),
    winston = require('winston');

const usersDatatable = function (aoData, clientRes) {

    const fieldsToSearch = ["nameFirst","nameLast"];
    const alias = "`user`";

    const dataQuery =
        "SELECT META(`user`).id as id, `user.`email, " +
        "`user`.nameFirst, `user`.nameLast "+
        "FROM dc_db `user` " +
        "WHERE `user`._sync AND " +
        "`user`.type = 'user' ";

    const recordCountQuery =
        "SELECT COUNT(META(`user`).id) as count " +
        "FROM dc_db `user` " +
        "WHERE `user`._sync AND " +
        "`user`.type = 'user' ";

    datatables.commonDatatable(dataQuery, recordCountQuery, alias, fieldsToSearch, aoData, clientRes);
};

const createUserCredentialsDoc = function (hash, userDoc, clientReq, clientRes) {

    const userCredentialsDoc = {
        "hashed_password": hash,
        "user_id": userDoc.id,
        "type": "userCredentials"
    };
    const id = uuid.v1();

    db.createDoc(id, userCredentialsDoc, function (err, res) {

        if (err) {
            clientRes.status(500).json({err:err, code:"U1"});
            winston.log('error', err);
            return;
        }

        clientRes.json(userDoc);
    });
};

const createUserDoc = function (hash, clientReq, clientRes) {

    const userDoc = documentStructure["user"](clientReq.body);
    const id = uuid.v1();

    db.createDoc(id, userDoc, function (err, res) {

        if (err) {
            clientRes.status(500).json({err:err, code:"U2"});
            winston.log('error', err);
            return;
        }

        userDoc["id"] = id;
        createUserCredentialsDoc(hash, userDoc, clientReq, clientRes);
    });
};

const hashPassword = function (clientReq, clientRes) {

    if (clientReq.body.password) {

        bcrypt.hash(clientReq.body.password, null, null, function (err, hash) {

            if (err) {
                clientRes.status(500).json({err:err, code:"U3"});
                winston.log('error', err);
                return;
            }

            /* set password attr in userDoc to null */
            delete clientReq.body.password;

            createUserDoc(hash, clientReq, clientRes);
        });
    } else {
        clientRes.status(500).json({err:"Password field needs to be filled out in order to create a user", code:"U4"});
        winston.log('error', 'Password field needs to be filled out in order to create a user');
    }
};

const isEmailUnique = function (clientReq, clientRes) {

    var query =
        "SELECT META(`user`).id AS id " +
        "FROM dc_db `user` " +
        "WHERE `user`.type = 'user' " +
        "AND `user`.email = '" + clientReq.body.email + "'";

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            clientRes.status(500).json({err:err, code:"U5"});
            winston.log('error', err);
            return;
        }

        // if there are any results with this email then it is not unique
        if (res.length > 0)
            clientRes.status(500).json({err:"Email is already in use", code:"U6"});
        else
            hashPassword(clientReq, clientRes);
    });
};

const getUsers = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'user', clientRes);
    else
        usersDatatable(clientReq.body.aoData, clientRes);
};

/* PUBLIC API */
module.exports = {
    createUser: isEmailUnique,
    getUsers: getUsers,
    updateUser: common.updateDocAndRespond,
    deleteUser: common.deleteDocAndRespond
};
