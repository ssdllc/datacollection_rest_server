
'use strict';

const common = require('../helpers/common.js'),
    n1qlQueryDefinition = require('../helpers/n1qlQueryDefinition.js'),
    datatables = require('../helpers/datatables.js'),
    db = require('../requests/db.js'),
    winston = require('winston');


const facilitiesDatatable = function (project, aoData, clientRes) {

    const fieldsToSearch = ["activityName", "facilityName", "facilityNumber", "nfaNumber", "state", "`value`"];
    const alias = "facility";

    const attributesToSelect = ["activityName","facilityName","facilityNumber","nfaNumber","state","`value`","title"];
    const whereClauseAttributeValues = {"type":"facility","project":project};

    const dataQuery = n1qlQueryDefinition.dataQuery(alias,attributesToSelect,whereClauseAttributeValues);

    const recordCountQuery = n1qlQueryDefinition.recordCountQuery(alias,whereClauseAttributeValues);

    //const dataQuery =
    //    "SELECT META(facility).id as id, facility.activityName, facility.facilityName, " +
    //    "facility.facilityNumber, facility.nfaNumber, facility.state, facility.`value`, facility.title " +
    //    "FROM dc_db facility " +
    //    "WHERE facility._sync AND " +
    //    "facility.type = 'facility' " +
    //    "AND facility.project = '" + project + "' ";
    //
    //const recordCountQuery =
    //    "SELECT COUNT(META(facility).id) as count " +
    //    "FROM dc_db facility " +
    //    "WHERE facility._sync AND " +
    //    "facility.type = 'facility' " +
    //    "AND facility.project = '" + project + "' ";

    datatables.commonDatatable(dataQuery, recordCountQuery, alias, fieldsToSearch, aoData, clientRes);
};

const getValues = function(qResults) {
    var retValues = [];
    for (var i=0; i < qResults.length; i++) {
        var rec = qResults[i];
        rec.value["id"]=rec.id;
        retValues.push(rec.value);
    }
    return retValues;
};

const facilitiesCollection = function(clientReq,clientRes){
    let viewQuery = db.couchbaseViewQuery;
    let projectID = clientReq.body.project;
    var query = viewQuery
        .from('lookUps', 'viewFacilities')
        .stale(viewQuery.Update.BEFORE)
        .range([projectID],[projectID,{}],true);


    db.viewQuery(query, function(err, results) {
        var resValues = getValues(results);
        clientRes.json(resValues);
    });
};

const getFacilities = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'facility', clientRes);
    else if (clientReq.body.aoData)
        facilitiesDatatable(clientReq.body.project, clientReq.body.aoData, clientRes);
    else
        facilitiesCollection(clientReq,clientRes);


};

/* PUBLIC API */
module.exports = {
    createFacility: common.createDocAndRespond,
    getFacilities: getFacilities,
    updateFacility: common.updateDocAndRespond,
    deleteFacility: common.deleteDocAndRespond
};
