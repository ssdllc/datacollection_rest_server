
'use strict';

const common = require('../helpers/common.js'),
    winston = require('winston');

const uniformatIIList = function (clientRes) {

    const query =
        "SELECT META(uniformatII).id as id, " +
        "uniformatII.* " +
        "FROM dc_db uniformatII " +
        "WHERE uniformatII.type = 'uniformatII'";

    common.n1qlRequestAndRespond(query, clientRes);
};

const getUniformatII = function (clientReq, clientRes) {

    if (clientReq.body.id)
        common.getDocAndRespond(clientReq.body.id, 'uniformatII', clientRes);
    else
        uniformatIIList(clientRes);
};

/* PUBLIC API */
module.exports = {
    getUniformatII: getUniformatII
};
