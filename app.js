
'use strict';

const login = require('./endPoints/login.js'),
    users = require('./endPoints/users.js'),
    projects = require('./endPoints/projects.js'),
    facilities = require('./endPoints/facilities.js'),
    assetInventory = require('./endPoints/assetInventory.js'),
    uniformatII = require('./endPoints/uniformatII.js'),
    materials = require('./endPoints/materials.js'),
    units = require('./endPoints/units.js'),
    config = require('./config.json'),
    express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    CouchbaseStore = require('connect-couchbase')(session),
    app = express(),
    winston = require('winston'),
    couchbaseStore = new CouchbaseStore({
        bucket:"dc_db",
        host: config.couchbaseIP + ":8091",
        connectionTimeout: 4000,
        operationTimeout: 4000,
        cachefile: '',
        ttl: 86400,
        prefix: 'sess'
    });
    winston.add(winston.transports.File, { filename: 'errors.log', level: 'error' });

couchbaseStore.on('connect', function() {
    console.log("Couchbase Session store is ready for use");
});


couchbaseStore.on('disconnect', function() {
    console.log("An error occurred connecting to Couchbase Session Storage");
    //hopefully pm2 restarts me after this fails
    process.exit();
});

// if nginxIP exists and https is true
// then set trust proxy to nginxIP
if (config.nginxIP && config.https) {
    app.set('trust proxy', function (ip) {
        if (ip === config.nginxIP) return true; // trusted IP
        else return false;
    });
}

/* used for session management */
app.use(session({
    store: couchbaseStore,
    name: "Data-Collection-Session",
    secret: config.sessionSecret,
    cookie: {
        httpOnly: true,
        secure: config.https,
        expires: false, //cookie will remain for the duration of the user-agent
        maxAge: 120*60*1000 //stay alive for 2 hours
    },
    resave: false, //false because i think couchbase implements the touch method
    saveUninitialized: true //not sure on this one, might be false because it is a login session, might save after i save the user_id to session?
}));

app.use(bodyParser.urlencoded({ extended: true }));

/* Sets the header of the response */
function setHeaders(req, res) {

    var allowOrigin = config.AccessControlAllowOrigin;

    if (allowOrigin.active === true) {
        var origins = allowOrigin.ips;
        for(var i=0;i<origins.length;i++){
            var origin = origins[i];
            if (req.headers.origin) {
                if (req.headers.origin.indexOf(origin) > -1) {
                    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
                    break;
                }
            }
            //else, tough cookies.
        }
    }
    res.setHeader("Access-Control-Allow-Credentials","true");
    res.setHeader('Content-Type', 'application/json');
}

app.use(function (req, res, next) { // all requests come here
    console.log(req.originalUrl + ' -- request received');
    setHeaders(req,res);
    next();
});
app.use(/^(?!.*\/log).*$/, function (req, res, next) { // all requests except /login and /logout come here

    login.loggedIn(req, function (err, bool) {

        if (err) {
            res.status(500).json(err);
            return;
        }

        if (bool) {
            next();
        }
        else
            res.status(403).send('Session expired. Please log in again.');
    });
});


/* REST ENDPOINTS */
app.post('/createUser', function(req,res){
    users.createUser(req, res);
});
app.post('/getUsers', function(req,res){
    users.getUsers(req, res);
});
app.post('/updateUser', function(req, res) {
    users.updateUser('user', req, res);
});
app.post('/deleteUser', function(req, res) {
    users.deleteUser('user', req, res);
});


app.post('/createProject', function(req, res) {
    projects.createProject('project', req, res);
});
app.post('/getProjects', function(req, res) {
    projects.getProjects(req, res);
});
app.post('/updateProject', function(req, res) {
    projects.updateProject('project', req, res);
});
app.post('/deleteProject', function(req, res) {
    projects.deleteProject('project', req, res);
});


app.post('/createFacility', function(req, res) {
    facilities.createFacility('facility', req, res);
});
app.post('/getFacilities', function(req, res) {
    facilities.getFacilities(req, res);
});
app.post('/updateFacility', function(req, res) {
    facilities.updateFacility('facility', req, res);
});
app.post('/deleteFacility', function(req, res) {
    facilities.deleteFacility('facility', req, res);
});


app.post('/createAssetInventory', function(req, res) {
    assetInventory.createFacility('assetInventory', req, res);
});
app.post('/getAssetInventory', function(req, res) {
    assetInventory.getAssetInventory(req, res);
});
app.post('/updateAssetInventory', function(req, res) {
    assetInventory.updateFacility('assetInventory', req, res);
});
app.post('/deleteAssetInventory', function(req, res) {
    assetInventory.deleteFacility('assetInventory', req, res);
});


app.post('/getUniformatII', function(req, res) {
    uniformatII.getUniformatII(req, res);
});


app.post('/getMaterials', function(req, res) {
    materials.getMaterials(req, res);
});


app.post('/getUnits', function(req, res) {
    units.getUnits(req, res);
});


app.post('/getCurrentUser', function(req, res) {
    login.getCurrentUser(req, res);
});
app.post('/login', function(req, res) {
    login.login(req, res);
});
app.post('/logout', function(req, res) {
    login.logout(req, res);
});


app.listen(process.env.PORT || 3412);
