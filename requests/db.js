
'use strict';

const config = require('../config.json'),
    request = require('request'),
    N1qlQuery = require('couchbase').N1qlQuery,
    couchbase = require("couchbase"),
    couchbaseViewQuery = couchbase.ViewQuery,
    dc_cluster = new couchbase.Cluster(config.couchbaseIP+':8091');


var loggedInUser = {};

const getLoggedInUser = function () {
    return loggedInUser;
};

const setLoggedInUser = function (user) {
    loggedInUser = user;
};


/* function that opens the bucket and talks to the database */
function dataAccess (type, callback, queryOrId, data) {

    var dc_db = dc_cluster.openBucket('dc_db', function(err) {
        if (err)
            callback(err);


        if (data) {
            dc_db[type](queryOrId, data, function(err, res) {
                callback(err, res);
            });
        } else {
            dc_db[type](queryOrId, function(err, res) {
                callback(err, res);
            });
        }
    });
}

/* Create, Update, or Delete a doc via sync gateway */
function cudRequest (requestMethod, uriAppend, data, callback) {
    request(
        {
            method: requestMethod,
            uri: 'http://admin:synergy1@'+config.couchbaseIP+':4984/dc_db/' + uriAppend,
            multipart: [
                {
                    'content-type': 'application/json',
                    body: JSON.stringify(data)
                }
            ]
        }
        , function (err, res, body) {
            callback(err, res, body);
        }
    );
}

/* Get a doc */
const getDoc = function (id, callback) {
    console.log('getDoc: ' + id);
    dataAccess('get', callback, id);
};

/* Create a doc */
const createDoc = function (id, data, callback) {
    console.log('createDoc: ' + id);
    cudRequest('PUT', id, data, function (err, res) {
        callback(err, res);
    });
};

/* Update a doc */
const updateDoc = function(id, syncRev, data, callback) {
    console.log('updateDoc: ' + id);
    const uriAppend = id+"?rev="+syncRev;
    delete data._sync;
    cudRequest('PUT', uriAppend, data, function (err, res) {
        callback(err, res);
    });
};

/* Delete a doc */
const deleteDoc = function(id, syncRev, callback) {
    console.log('deleteDoc: ' + id);
    const uriAppend = id+"?rev="+syncRev;
    cudRequest('DELETE', uriAppend, {}, function (err, res) {
        callback(err, res);
    });
};

/* View query to the database */
const viewQuery = function (query, callback) {
    console.log('viewQuery: ' + query);
    dataAccess('query', callback, query);
};

/* N1ql query to the database */
const n1qlRequest = function  (query, callback) {
    // .consistency(N1qlQuery.Consistency.REQUEST_PLUS); <-- this is what makes stale = false
    console.log('n1qlRequest: ' + query);
    query = N1qlQuery.fromString(query).consistency(N1qlQuery.Consistency.REQUEST_PLUS);
    dataAccess('query', callback, query);
};

/* PUBLIC API */
module.exports = {
    getDoc: getDoc,
    createDoc: createDoc,
    updateDoc: updateDoc,
    deleteDoc: deleteDoc,
    viewQuery: viewQuery,
    couchbaseViewQuery: couchbaseViewQuery,
    n1qlRequest: n1qlRequest,
    getLoggedInUser: getLoggedInUser,
    setLoggedInUser: setLoggedInUser
};