
'use strict';

const db = require('../requests/db.js'),
    Rx = require('rx'),
    n1qlRequest = Rx.Observable.fromNodeCallback(db.n1qlRequest);


function toLowerString (alias/*, fieldname*/) {

    return 'LOWER(' + alias + '.';
    //
    //var lowerString = "LOWER(x.";
    //
    //if (whereClause.startsWith("per.type = 'person'")) {
    //    lowerString = "LOWER(per.";
    //}
    //else if (fieldname == "state.`value`"
    //    || fieldname == "y.nameFirst || ' ' || y.nameLast"
    //    || fieldname == "state.display || ' ' || pro.title"
    //    || fieldname == "z.display") {
    //    lowerString = "LOWER(";
    //}
    //
    //return lowerString
}

const setSearch = function (alias, aoDataDictionary, fieldsToSearch, aoData) {

    let searchString = "";

    /* the value returned from search field */
    let searchParam =  aoData[aoDataDictionary.search].value.value;

    /* If there are items in the search field create wildcard query */
    if (!(searchParam == "")) {

        /* change searchParam to lowercase */
        searchParam = searchParam.toLowerCase();

        /* Create whereClause */
        for(let j = 0; j < fieldsToSearch.length; j++) {

            var currentField = fieldsToSearch[j];
            var lowerString = toLowerString(alias/*, currentField*/) + currentField;

            if((j==0) && ((j+1) == fieldsToSearch.length)) //first and last run, only need AND
                searchString += " AND (" + lowerString + ") LIKE '%" + searchParam + "%')";
            else if(j==0) //first run, need AND and OR
                searchString += " AND (" + lowerString + ") LIKE '%" + searchParam + "%' OR";
            else if((j+1) == fieldsToSearch.length) //last run, don't need OR or AND
                searchString += " " + lowerString + ") LIKE '%" + searchParam + "%')";
            else //middle run, need only OR
                searchString += " " + lowerString + ") LIKE '%" + searchParam + "%' OR";
        }
    }
    return searchString;
};

const setOrderBy = function (alias, aoDataDictionary, aoData) {

    let orderByString = "ORDER BY ";

    /* if on last run don't return a comma, otherwise return a comma */
    function commaOrNotString (i) {
        if (aoData[aoDataDictionary.order].value.length === (i+1))
            return " ";
        else
            return ", ";
    }

    function editOrderParam (orderParam) {

        //else if (orderParam == "programDisplay" || orderParam == "hospitalDisplay"
        //    || orderParam == "stateAbbreviation" || orderParam == "userDisplay")
        //    return orderParam;
        //else
            return alias + ".`" + orderParam + "`";
    }

    for (let i = 0; i < aoData[aoDataDictionary.order].value.length; i++) {

        /* asc vs desc value */
        let ordering = aoData[aoDataDictionary.order].value[i].dir;

        /* the index for the name ordered by */
        let tempOrderIndex = aoData[aoDataDictionary.order].value[i].column;

        /* determines whether or not the column type is a number, if number need to change query to orderBy properly */
        let columnType = aoData[aoDataDictionary.columns].value[tempOrderIndex].name;

        /* uses the index from above to retrieve the name ordered by */
        let orderParam = aoData[aoDataDictionary.columns].value[tempOrderIndex].data;

        orderParam = editOrderParam(orderParam);

        if(columnType === "number")
            orderByString += orderParam + " " + ordering + commaOrNotString(i);
        else
            orderByString += "LOWER(" + orderParam + ") " + ordering + commaOrNotString(i);
    }
    return orderByString;
};

const setPagination = function (aoDataDictionary, aoData) {

    const lengthParam = aoData[aoDataDictionary.length].value;
    const startParam = aoData[aoDataDictionary.start].value;

    return "LIMIT "+ lengthParam + " OFFSET "+ startParam;
};

const createAoDataDictionary = function (aoData) {

    let aoDataDictionary = {};

    if (aoData) {
        for (let i = 0; i < aoData.length; i++) {
            aoDataDictionary[aoData[i].name] = i;
        }
    }
    return aoDataDictionary;
};

const queryAndRespond = function (dataQuery, recordCountQuery, clientRes) {

    const s1 = n1qlRequest(dataQuery);
    const s2 = n1qlRequest(recordCountQuery);

    const combination = Rx.Observable.zip(s1, s2, function (s1, s2) {
        return {
            data: s1,
            recordCount: s2
        };
    });

    let finishedObj = {};

    const logObserver = Rx.Observer.create(

        function (result) {
            finishedObj = result;
        },

        function (err) {
            clientRes.status(500).json({err:err, code:"DT0"});
        },

        function () {

            const data = finishedObj.data;
            const recordCount = finishedObj.recordCount;

            /* respond to client */
            if(recordCount.length > 0)
                clientRes.json({"recordCount":recordCount[0].count,"data":data});
            else
                clientRes.json({"recordCount":0,"data":data});
        }
    );
    combination.subscribe(logObserver);
};

const commonDatatable = function (dataQuery, recordCountQuery, alias, fieldsToSearch, aoData, clientRes) {

    const aoDataDictionary = createAoDataDictionary(aoData);
    const setSearchString = setSearch(alias, aoDataDictionary, fieldsToSearch, aoData);

    dataQuery += setSearchString +
        setOrderBy(alias, aoDataDictionary, aoData) +
        setPagination(aoDataDictionary, aoData);

    recordCountQuery += setSearchString;

    queryAndRespond(dataQuery, recordCountQuery, clientRes);
};


/* PUBLIC API */
module.exports = {
    setSearch: setSearch,
    setOrderBy: setOrderBy,
    setPagination: setPagination,
    createAoDataDictionary: createAoDataDictionary,
    queryAndRespond: queryAndRespond,
    commonDatatable: commonDatatable
};
