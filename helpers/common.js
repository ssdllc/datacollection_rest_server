
'use strict';

const uuid = require('node-uuid'),
    db = require('../requests/db.js'),
    documentStructure = require('../helpers/documentStructure.js');

const getDoc = function (id, type, callback) {

    db.getDoc(id, function(err, res) {
        if (err) {
            callback({err:err, code:"C1"}, null);
            return;
        }
        if (res.value.type !== type) {
            callback({err:"The requested document does not match the document type requested.", code:"C2"}, null);
            return;
        }
        callback(null, res.value);
    });
};

const createDocAndRespond = function (type, clientReq, clientRes) {

    const data = documentStructure[type](clientReq.body);
    const id = uuid.v1();

    db.createDoc(id, data, function (err, res) {

        if (err) {
            clientRes.status(500).json({err:err, code:"C3"});
            return;
        }

        data["id"] = id;
        clientRes.json(data);
    });
};

const getDocAndRespond = function (id, type, clientRes) {

    getDoc(id, type, function (err, res) {

        if (err) {
            clientRes.status(500).json(err);
            return;
        }

        res["id"] = id;
        clientRes.json(res);
    });
};

const updateDocAndRespond = function (type, clientReq, clientRes) {

    const id = clientReq.body.id;

    getDoc(id, type, function (err, res) {

        if (err) {
            clientRes.status(500).json(err);
            return;
        }

        const syncRev = res._sync.rev;
        const data = documentStructure[type](clientReq.body);

        db.updateDoc(id, syncRev, data, function (err, res) {

            if (err) {
                clientRes.status(500).json({err:err, code:"C4"});
                return;
            }

            data["id"] = id;
            clientRes.json(data);
        });
    });
};

const deleteDocAndRespond = function (type, clientReq, clientRes) {

    const id = clientReq.body.id;

    getDoc(id, type, function (err, res) {

        if (err) {
            clientRes.status(500).json(err);
            return;
        }

        const syncRev = res._sync.rev;

        db.deleteDoc(id, syncRev, function (err, res) {

            if (err) {
                clientRes.status(500).json({err:err, code:"C5"});
                return;
            }

            clientRes.json({"message":"document deleted"});
        });
    });
};

const n1qlRequestAndRespond = function (query, clientRes) {

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            clientRes.status(500).json({err:err, code:"C6"});
            return;
        }
        clientRes.json(res);
    });
};

const viewQueryGetValues = function (viewQueryArray) {
    let retValues = [];
    for (let i=0; i < viewQueryArray.length; i++) {
        let rec = viewQueryArray[i];
        retValues.push(rec.value)
    }
    return retValues;
};

/* PUBLIC API */
module.exports = {
    createDocAndRespond: createDocAndRespond,
    getDocAndRespond: getDocAndRespond,
    updateDocAndRespond: updateDocAndRespond,
    deleteDocAndRespond: deleteDocAndRespond,
    n1qlRequestAndRespond: n1qlRequestAndRespond,
    viewQueryGetValues: viewQueryGetValues
};

