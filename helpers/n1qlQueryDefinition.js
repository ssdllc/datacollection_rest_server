'use strict';

const selectStatementBuilder = function(alias, attributes){
    let attributesLength = attributes.length;
    let selectStatement = "SELECT META("+alias+").id as id, ";
    // Build Select Statement
    for(let i = 0 ; i < attributesLength ; i++){
        selectStatement = selectStatement + alias+"."+attributes[i]+", ";
        if (i == attributesLength - 1){
            selectStatement = selectStatement.slice(0, -2);
            selectStatement = selectStatement + " ";
        }
    }
    return selectStatement;
};

const whereStatementBuilder = function(alias, whereClause){
    let whereStatement = "WHERE "+alias+"._sync AND ";
    let whereClauseLength = Object.keys(whereClause).length;
    let jsonIterator = 0;
    for (var key in whereClause) {
        if (whereClause.hasOwnProperty(key)) {
            jsonIterator ++;
            whereStatement = whereStatement + alias+"."+key+" = '"+whereClause[key]+"' AND ";
            if(jsonIterator == whereClauseLength)
                whereStatement = whereStatement.slice(0, -4);

        }
    }
    return whereStatement;
};

const dataQuery = function(alias, attributes, whereClause){
    // Select Statement
    const selectStatement = selectStatementBuilder(alias, attributes);

    // From Statement
    const fromStatement = "FROM dc_db "+alias+" ";

    // Build Where Statement
    const whereStatement = whereStatementBuilder(alias, whereClause);

    // Concatenate all the statements
    let dataStatementQuery = selectStatement+fromStatement+whereStatement;

    return dataStatementQuery;
};

const recordCountQuery = function(alias, whereClause){
    // Select Statement
    const selectStatement = "SELECT COUNT(META("+alias+").id) as count ";

    // From Statement
    const fromStatement = "FROM dc_db "+alias+" ";

    // Build Where Statement
    const whereStatement = whereStatementBuilder(alias, whereClause);

    // Concatenate all the statements
    const recordCountStatementQuery = selectStatement+fromStatement+whereStatement;

    return recordCountStatementQuery;
};

/* PUBLIC API */
module.exports = {
    dataQuery: dataQuery,
    recordCountQuery: recordCountQuery
};