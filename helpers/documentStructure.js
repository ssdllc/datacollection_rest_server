
'use strict';

function setProperty (obj, prop, empty) {

    if (obj[prop])
        return obj[prop];

    if (empty === "array")
        return [];
    else if (empty === "object")
        return {};
    else if (empty === "number")
        return null;

    return "";
}

const project = function (data) {
    return {
        "title": setProperty(data, "title"),
        "date": setProperty(data, "date"),
        "location": setProperty(data, "location"),
        "value": setProperty(data, "value"),
        "type": "project",
        "channels": setProperty(data, "channels", "array")
    };
};

const facility = function (data) {
    return {
        "activityName":setProperty(data, "activityName"),
        "facilityName":setProperty(data, "facilityName"),
        "facilityNumber":setProperty(data, "facilityNumber"),
        "nfaNumber":setProperty(data, "nfaNumber"),
        "state":setProperty(data, "state"),
        "title":setProperty(data, "title"),
        "project": setProperty(data, "project"),
        "value": setProperty(data, "value"),
        "type": "facility",
        "channels": setProperty(data, "channels", "array")
    };
};

const user = function (data) {
    return {
        "nameFirst":setProperty(data, "nameFirst"),
        "nameLast":setProperty(data, "nameLast"),
        "email":setProperty(data, "email"),
        "type": "user",
        "channels": setProperty(data, "channels", "array")
    };
};


const assetInventory = function (data) {
    return {
        data: setProperty(data, "data", "array"),
        facility: setProperty(data, "facility"),
        number: setProperty(data, "number", "number"),
        uniformatII_ID: setProperty(data, "uniformatII_ID"),
        type: "data_assetInventory",
        "channels": setProperty(data, "channels", "array")
    };
};


/* PUBLIC API */
module.exports = {
    project: project,
    facility: facility,
    user: user,
    assetInventory: assetInventory
};

