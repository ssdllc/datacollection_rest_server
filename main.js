var uuid = require('node-uuid');
var db = require('./requests/db.js');

/////CREATE DOCUMENTS
function createDoc(data, resObj) {
    var newID = uuid.v1();
    data.channels = ["ssd"];
    delete data.id;

    db.cudRequest('PUT', newID, data, function (err, res, body) {

        if(res.statusCode == 201){
            console.log('document saved');
            if (resObj) { //this is what saves u isn't it?
                if (data.type == "facility") {
                    resObj = response.respond(resObj,
                        {"status": "success", "id": newID, "project": data.project});

                } else if (data.type == "stretchSection") {
                    resObj = response.respond(resObj,
                        {"status": "success", "id": newID, "project": data.facility});

                } else {
                    resObj = response.respond(resObj, {"status": "success", "id": newID});
                }
            }
        } else {
            console.log('error: '+ res.statusCode);
            console.log(body);
            resObj = response.respond(resObj, {"status":"error: "+res.statusCode});
        }
    });
}

////UPDATE OR DELETE DOCUMENTS/////
function pushDoc(pushType, id, data, resObj) {

    db.getDoc(id, function(err, res) {
        if (err) {
            console.log("operation failed", err);
            return;
        }
        var requestMethod;
        if (pushType=="delete") {
            requestMethod = "DELETE";
        } else {
            requestMethod = "PUT";
            delete data.id;
            data.type = res.value.type;
            data.channels = [res.value.channels];
        }

        //var requestMethod = (pushType=="delete") ? "DELETE" : "PUT";
        var uriAppend = id+"?rev="+res.value._sync.rev;
        //var uriAppend = (pushType=="create") ? "" : id+"?rev="+res.value._sync.rev;

        db.cudRequest(requestMethod, uriAppend, data, function (err, res, body) {

            if(res.statusCode == 201 || res.statusCode == 200) {
                console.log('document saved');
                resObj = response.respond(resObj, {"status":"success","ID":id});
            } else {
                console.log('error: '+ res.statusCode);
                console.log(body);
                resObj = response.respond(resObj, {"status":"error: "+res.statusCode});
            }
        });
    });
}

function getValues(qResults) {
    var retValues = [];
    for (var i=0; i < qResults.length; i++) {
        var rec = qResults[i];
        retValues.push(rec.value)
    }
    return retValues;
}

//////LOCAL CRUD FUNCTIONS//////////
var lc_create = function(resObj,data) {
    var reqObj = JSON.parse(data);
    var newVals = (reqObj.data) ? reqObj.data[0] : {};
    switch(reqObj.module) {
        case "projectManager":
            switch (reqObj.target) {
                case "project":
                    createDoc(newVals,resObj);
                    break;
                case "facility":
                    var facID = createDoc(newVals,resObj);
                    createDoc({ //what the heck is this?
                        "type": "data_pileConfiguration",
                        "facility": facID,
                        "order": 0,
                        "bent": "1",
                        "rows": [{"order": 0,"row": "A"}]
                    });
                    break;
                case "stretchSection":
                    createDoc(newVals,resObj);
                    break;
            }
            break;
    }
    return "create";
};
var lc_read = function(resObj, data) {
    var reqObj = JSON.parse(data);
    var viewQuery = db.couchbaseViewQuery;

    switch(reqObj.module) {
        case "projectManager":
            switch(reqObj.target) {
                case "projects":
                    var query = viewQuery
                        .from('lookUps', 'viewProjects')
                        .stale(viewQuery.Update.BEFORE);


                    db.viewQuery(query, function(err, results) {
                        var resValues = getValues(results);
                        resObj = response.respond(resObj, resValues);
                    });
                    break;
                case "facilitiesByProject":
                    var projectID = reqObj.data[0].id;
                    var query = viewQuery
                        .from('lookUps', 'viewFacilities')
                        .stale(viewQuery.Update.BEFORE)
                        .range([projectID],[projectID,{}],true);
                        //.stale(viewQuery.Update.AFTER) //.custom(options); //.key(projectID); //

                    db.viewQuery(query, function(err, results) {
                        var resValues = getValues(results);
                        resObj = response.respond(resObj, resValues);
                    });
                    break;
                case "stretchSectionsByFacility":
                    var facilityID = reqObj.data[0].id;
                    var query = viewQuery
                        .from('lookUps', 'viewStretchSection')
                        .stale(viewQuery.Update.BEFORE)
                        .range([facilityID],[facilityID,{}],true);
                        //.stale(viewQuery.Update.AFTER) //.custom(options); //.key(projectID); //

                    db.viewQuery(query, function(err, results) {
                        var resValues = getValues(results);
                        resObj = response.respond(resObj, resValues);
                    });
                    break;
                case "pileConfigurationsByFacility":

                    var facilityID = reqObj.data[0].id;
                    var query = viewQuery
                        .from('lookUps', 'viewPileConfig')
                        .stale(viewQuery.Update.BEFORE)
                        .range([facilityID],[facilityID,{}],true);
                    //.stale(viewQuery.Update.AFTER) //.custom(options); //.key(projectID); //

                    db.viewQuery(query, function(err, results) {
                        var resValues = getValues(results);
                        resObj = response.respond(resObj, resValues);
                    });

                    break;
                case "modelRequest":
                    var docID = reqObj.data[0].id;
                    db.getDoc(docID, function(err, results) {
                        var returnVal;
                        if(err) {
                            returnVal = '{"status":"model request error"}';
                        } else {
                            returnVal = '{"status":"nothing found"}';
                            if (results)
                                if (results.value)
                                    returnVal = results.value;
                        }
                        if(typeof returnVal == "string")
                            resObj = response.respond(resObj, returnVal);
                        else
                            resObj = response.respond(resObj, returnVal, true, true);

                    });
                    break;
            }
            break;
    }
    return "read";
};
var lc_update = function(resObj,data) {
    var reqObj = JSON.parse(data);
    var newVals = (reqObj.data) ? reqObj.data[0] : {};
    switch(reqObj.module) {
        case "projectManager":
            switch (reqObj.target) {
                case "project":
                    pushDoc("update",newVals.id,newVals,resObj);
                    break;
                case "facility":
                    pushDoc("update",newVals.id,newVals,resObj);
                    break;
                case "stretchSection":
                    pushDoc("update",newVals.id,newVals,resObj);
                    break;
            }
            break;
    }
    return "update";
};
var lc_delete = function(resObj,data) {

    var reqObj = JSON.parse(data);
    var newVals = (reqObj.data) ? reqObj.data[0] : {};
    pushDoc("delete",newVals.id,{},resObj);

    return "delete";
};

var test = function (res,id) {

    var viewQuery = db.couchbaseViewQuery;

    var projectID = "a581a110-c0e4-11e4-85cc-5b31063c31ef";
    var facilityID = "34a6fc00-c0e5-11e4-a65a-3d7a85e50c6e";

    var options = {
        //key: projectID //,
        //endKeyDocId: projectID,
        //limit: 1
        //startKeyDocId:projectID,
        //endKeyDocId:projectID,

        //startKey:[projectID],
        //endKey:[projectID],
        inclusive_end: true
        //stale: false
    };

    var query = viewQuery
        .from('lookUps', 'viewFacilities')
        .range([projectID],[projectID,{}],true)
        .stale(viewQuery.Update.BEFORE)
        .custom(options); //.key(projectID); //

    //query.custom(options);

    db.viewQuery(query, function(err, results) {
        //var resValues = getValues(results);
        if (err) {
            console.log(err);
        } else {
            console.log(results);
        }
    });


    /*
     //var buck = dc_db;
     var query = viewQuery.from('lookUps', 'byType');
     var executedQuery = dc_db.query(query);
     for (i in executedQuery) {
     console.log(executedQuery[i]);
     }

     db.viewQuery(query, function(err, results) {
     //for(i in results)
     //    console.log(results[i]);

     res.json(results[id]);
     });
     */
};




////PUBLIC API//////////////
module.exports.getCreate = function(resObj,data) {

    return lc_create(resObj,data);
};
module.exports.getRead = function(resObj,data) {
    return lc_read(resObj,data);
};
module.exports.getUpdate = function(resObj,data) {
    return lc_update(resObj,data);
};
module.exports.getDelete = function(resObj,data) {
    return lc_delete(resObj,data);
};

module.exports.getTest = function(abc,id) {
    return test(abc,id);
};